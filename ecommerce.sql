--users
	--id
	--userName
	--email
	--password
	--isAdmin
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	isAdmin BOOLEAN DEFAULT FALSE,
	PRIMARY KEY(id)
);

--products
	--id
	--name
	--description
	--price
	--category_id
CREATE TABLE products(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT NOT NULL,
	price DECIMAL(10,2) NOT NULL,
	img_path VARCHAR(255),
	PRIMARY KEY(id),
	category_id INT NOT NULL,
	FOREIGN KEY(category_id)
	REFERENCES categories(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

--categories
	--id
	--name
CREATE TABLE categories(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

--orders
	--id
	--total_price
	--date
	--user_id
CREATE TABLE orders(
	id INT NOT NULL AUTO_INCREMENT,
	total DECIMAL(10,2) NOT NULL,
	dateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	userId INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(userId)
	REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

--products_orders
	--id
	--order_id
	--product_id
	--quantity
	--subtotal
CREATE TABLE products_orders(
	id INT NOT NULL AUTO_INCREMENT,
	orderId INT NOT NULL,
	productId INT NOT NULL,
	quantity INT NOT NULL,
	subtotal DECIMAL(10,2) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(orderId)
	REFERENCES orders(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(productId)
	REFERENCES products(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);