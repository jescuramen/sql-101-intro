Data Definition Language*/

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);


CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	year YEAR(4),
	artist_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(artist_id)
	REFERENCES artists(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	length INT,
	genre VARCHAR(255),
	album_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(album_id)
	REFERENCES albums(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	user_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id)
	REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT	
);

CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_id INT,
	playlist_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(song_id)
	REFERENCES songs(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(playlist_id)
	REFERENCES playlists(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT	
);

ALTER TABLE <table name> <table modification>
available table modification
ADD COLUMN <column name>
DROP COLUMN <column name>
RENAME TO <new table name>

ALTER TABLE playlists ADD COLUMN test VARCHAR(255);

ALTER TABLE playlists DROP COLUMN test;


/*DML - Data Manipulation Language*/

/*INSERT - create a new row in our database*/
INSERT INTO table(column1, column2, column3,...) VALUES(val1, val2, val3);

INSERT INTO artists(name) VALUES ("Andrew E");
INSERT INTO artists(name) VALUES ("Westlife");

INSERT INTO albums(name, year, artist_id) VALUES ("Banyo Queen", 1997, 3);
INSERT INTO albums(name, year, artist_id) VALUES ("Pink Palaka", 1998, 3);
INSERT INTO albums(name, year, artist_id) VALUES ("Coast to Coast", 1998, 4);

/*Multiple row insertion in a single statement*/
INSERT INTO songs(name, length, genre, album_id) values
	("My Love", 200, "pop", 3),
	("Close", 215, "pop", 3),
	("Against All Odds", 310, 'pop', 3);

INSERT INTO songs(name, length, genre, album_id) values
	("Pink Palaka", 280, "opm", 2),
	("Humanap ka ng Panget", 297, "opm", 2),
	("Banyo Queen", 310, 'opm', 2);	

/*SELECT - retrieves the specified columns in a table where a given condition is met*/
SELECT <columns> FROM table WHERE <condition>;

SELECT id, name FROM artists WHERE name = "Andrew E";

SELECT * FROM albums;

/*write an SQL query that will retrieve the titles(name) of songs with genre OPM*/
SELECT name FROM songs WHERE genre = "opm";

/*write a query that will retrieve the titles(name) and lengths of OPM songs that are more than 3 minutes*/
SELECT name, lengths FROM songs WHERE genre = "opm" and length > 180;


/*UPDATE*/
UPDATE table SET column1=newVal1, column2=newVal2, etc. WHERE <condition>;

/*query that will update all songs with genre = 'opm' to instead have genre = 'rap*/
UPDATE songs SET genre = "rap" WHERE genre = "opm";

/*query that will target a song titled 'Pink Palaka' and update its length to 310*/
UPDATE songs SET length = 310 WHERE name = "Pink Palaka";

/*DELETE*/
DELETE FROM table WHERE <condition>;
/*delete from table artists where id = 2 ('Andre E')*/
DELETE FROM artists WHERE id = 2;

/*Activity for DDL & DML*/
/*DDL*/
/*1. create a Database named studentDB*/
CREATE DATABASE studentDB;

/*2. studentDB will have the ff. tables and corresponding columns with data types enclosed in ():*/
	/*-students
		-id (int auto-increments)
		-firstName (varchar)
		-lastName (varchar)*/
CREATE TABLE students (
	id INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(255) NOT NULL,
	lastName VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);
	/*-activities
		-id (int auto-increments)
		-name (varchar)
		-price (decimal with 6 total digits, 2 decimal places)*/
CREATE TABLE activities (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	price DECIMAL(6, 2),
	PRIMARY KEY(id)
);
	/*-participants (pivot table connecting students to activities)
		-id (int auto-increments)
		-activity_id (int)
		-student_id (int)*/
CREATE TABLE participants (
	id INT NOT NULL AUTO_INCREMENT,
	activity_id INT,
	student_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(activity_id)
	REFERENCES activities(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY(student_id)
	REFERENCES students(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT	
);
/*DML*/
3. /*insert the ff. records in the students table (lastName, firstName):
	-Gaffud, Terrence
	-Cahilog, Sylvan
	-Mena, Alex
	-Cavas, Ben
	-Beraquit, Alan*/
INSERT INTO students(lastName, firstName) VALUES 
	("Gaffud","Terrence"),
	("Cahilog", "Sylvan"),
	("Mena", "Alex"),
	("Cavas", "Ben"),
	("Beraquit", "Alan");


4. /*insert the ff. records in the activities table(name, price):
	-golf, 599.85
	-sailing, 2012.74
	-squash, 426.17
	-swimming, 645.23
	-tennis, 845.28*/
INSERT INTO activities(name, price) VALUES 
	("golf", 599.85),
	("sailing", 2012.74),
	("squash", 426.17),
	("swimming", 645.23),
	("tennis", 845.28);

5. /*insert records in the participants pivot table that will reflect the following:
	-Ben Cavas went swimming and sailing
	-Allan Beraquit played golf, squash, and tennis
	-Terrence Gaffud went swimming and played squash
	-Alex Mena went sailing
	-Sylvan Cahilog did all activities*/
INSERT INTO participants(activity_id, student_id) VALUES
	(4, 4),
	(2, 4),
	(1, 5),
	(3, 5),
	(5, 5),
	(4, 1),
	(3, 1),
	(2, 3),
	(1, 2),
	(2, 2),
	(3, 2),
	(4, 2),
	(5, 2);

/*6. update last name of Alex Mena to Menya and his first name to Alexandro*/
UPDATE students SET firstName = "Alexandro" WHERE firstName = "Alex";

UPDATE students SET lastName = "Menya" WHERE lastName = "Mena";

/*7. write a query that will retrieve all activities with prices higher than 500.00*/
SELECT name, price FROM activities WHERE price > 500;

/*8. write a query to determine the student id of the student who registered for all activities*/
SELECT student_id FROM participants JOIN activities ON (
	activity_id = 1 and 
	activity_id = 2 and
	activity_id = 3 and
	activity_id = 4 and
	activity_id =5);


/*Queries 2 Advance Selects 2*/

/*Like Operator*/
/* 'B%' - starting with B */
/* '%B' - ending with B */
/* '%B%' - containing B */
/* '_____' - returns name with at least 5 characters */
SELECT * FROM pet WHERE name LIKE 'B%';

SELECT aut_name, country FROM author WHERE aut_name LIKE 'W%';
/*
William Norton
William Maugham
William Anthony
*/

SELECT aut_name, country FROM author WHERE aut_name LIKE '%on';
/*
William Norton
Thomas Merton
Piers Gibson
Joseph Milton
*/

SELECT aut_name, country, home_city FROM author WHERE home_city LIKE 'L_n_on';
/*
London
London
*/

SELECT aut_name, country FROM author WHERE aut_name LIKE 't%n';
/*
Thomas Merton
Thomas Morgan
*/

SELECT book_name, isbn_no, no_page, book_price FROM book_mast WHERE isbn_no LIKE '%\_16%';
/*
Networks and Telecommunications
00009790_16
95
45.00
*/

/*The JOIN keyword*/
SELECT name, title FROM albums JOIN songs ON (albums.id = songs.album_id);

/*Aggregate Functions*/

/*SUM*/
SELECT SUM(length) FROM songs;

/*AVG*/
SELECT AVG(length) FROM songs WHERE genre = 'Rap'; /*305.667*/

SELECT AVG(length) FROM songs WHERE genre = 'Pop'; /*241.667*/

/*GROUP BY*/
SELECT AVG(length), genre
FROM songs
GROUP BY genre;

/*COUNT*/
SELECT COUNT(name) FROM songs;

/*MAX*/
SELECT MAX(length) FROM songs WHERE genre = 'rap';

/*MIN*/
SELECT MIN(length) FROM songs WHERE genre = 'pop';

/*HAVING - is used in GROUP BY, because WHERE will not work on GROUP BY*/ 
SELECT AVG(length), genre FROM songs
GROUP BY genre
HAVING AVG(length) < 250;

/*Write a SQL statement to find the total purchase amount of all orders*/
SELECT SUM(purch_amt) FROM orders;
/*Write a SQL statement to find the average purchase amount of all orders*/
SELECT AVG(purch_amt) FROM orders;
/*Write a SQL statement to know how many customers have listed their names*/
SELECT COUNT(*) FROM customer;
SELECT COUNT(cust_name) FROM customer;
SELECT COUNT(DISTINCT cust_name) FROM customer; /*use DISTINCT if there are duplicate names*/