SQL Classic Models Activity:

Write the SQL queries that will return the results to answer the following questions:

/*1. return the customerName of the customers who are from the Philippines*/
SELECT customerName
FROM customers
WHERE country = 'Philippines';

/*2. return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"*/
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = 'La Rochelle Gifts';

/*3. return the product name and MSRP of the product named "The Titanic"*/
SELECT productName, MSRP
FROM products
WHERE productName = 'The Titanic';

/*4. return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"*/
SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

/*5. return the names of customers who have no registered state*/
SELECT customerName, state
FROM customers
WHERE state IS NULL;

/*6. return the first name, last name, email of the employee whose last name is Patterson and first name is Steve*/
SELECT firstName, lastName
FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

/*7. return customer name, country, and credit limit of customers whose countries are NOT USA
	and whose credit limits are greater than 3000*/
SELECT customerName, country, creditLimit
FROM customers
WHERE country != 'USA' AND creditLimit > 3000;

/*8. return the product name and quantity in stock of products that belong to the
product line "planes" with stock quantities less than 1000*/
SELECT productName, quantityInStock
FROM products
WHERE productLine = 'planes' AND quantityInStock < 1000;

/*9. return the customer names of customers whose customer names don't have 'a' in them*/
SELECT customerName
FROM customers
WHERE customerName NOT LIKE '%a%';

/*10. return the customer numbers of orders whose comments contain the acronym 'DHL'*/
SELECT customerNumber, comments
FROM orders
WHERE comments
LIKE '%DHL%';

/*11. return the product lines whose text description mentions the phrase 'state of the art'*/
SELECT productLine
FROM productlines
WHERE textDescription
LIKE '%state of the art%';

/*12. return the countries of customers without duplication*/
SELECT DISTINCT country
FROM customers;

/*13. return the statuses of orders without duplication*/
SELECT DISTINCT status
FROM orders;

/*14. return the customer names and countries of customers whose country is USA, France, or Canada:*/
SELECT customerName, country
FROM customers
WHERE country IN('USA', 'France', 'Canada');

/*15. return the first name, last name, and office's city of employees whose offices are in Tokyo*/
SELECT firstName, lastName, city
FROM employees
JOIN offices ON(offices.officeCode = employees.officeCode) WHERE city = 'Tokyo';

/*16. return the customer names of customers who were served by the employee named "Leslie Thompson"*/
SELECT customerName
FROM customers
JOIN employees ON(employees.employeeNumber = customers.salesRepEmployeeNumber)
WHERE firstName = 'Leslie' and lastName = 'Thompson';

/*17. return the product names and customer name of products ordered by "Baane Mini Imports"*/
SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails ON(products.productCode = orderdetails.productCode)
JOIN orders ON(orders.orderNumber = orderdetails.orderNumber)
JOIN customers ON(customers.customerNumber = orders.customerNumber)
WHERE customerName = "Baane Mini Imports";

-- 18. return the employees' first names, employees' last names, customers' names, and offices'
-- countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, customers.country
FROM offices
JOIN employees ON (offices.officeCode = employees.officeCode)
JOIN customers ON (employees.employeeNumber = customers.salesRepEmployeeNumber)
WHERE offices.country = customers.country;

-- 19. return the last names and first names of employees being supervised by "Anthony Bow"
-- query 1
SELECT lastName, firstName
FROM employees
WHERE reportsTo = 1143;

-- query 2
SELECT lastName, firstName
FROM employees
WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE lastName = 'Bow' AND firstName = 'Anthony');

--20. return the product name and MSRP of the product with the highest MSRP
------1952 Alpine Renault 1300 | 214.30--------
SELECT productName, MSRP
FROM products
WHERE MSRP = (SELECT MAX(MSRP) FROM products);

--21. return the number of customers in the UK
------5------
SELECT COUNT(country)
FROM customers
WHERE country = 'UK';

--22. return the number of products per product line
------------------------------
-- # of products | productLine
-- 38            | Classic Cars
-- 13            | Motorcycles
-- 12            | Planes
-- 9             | Ships
-- 3             | Trains
-- 11            | Trucks and Buses
-- 24            | Vintage Cars

SELECT COUNT(productName), productLine
FROM products
GROUP BY productLine;

-- 23. return the number of customers served by every employee
-- ----------------------------
-- # of customers | employee first name | employee last name
-- 5              | Andy                | Fixter
-- 9              | Barry               | Jones
-- 7              | Foon Yue            | Tseng
-- 8              | George              | Vanauf
-- 7              | Gerard              | Hernandez
-- 6              | Julie               | Firrelli
-- etc, etc.

SELECT COUNT(customers.customerName), employees.firstName, employees.lastName
FROM employees JOIN customers ON(employees.employeeNumber = customers.salesRepEmployeeNumber)
GROUP BY employees.employeeNumber;
